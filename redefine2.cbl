       IDENTIFICATION DIVISION. 
       PROGRAM-ID. REDEFINE2.
       AUTHOR. PISIT.

       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01  PercentToApply    PIC 9(3).
       01  Percentage REDEFINES PercentToApply    PIC 9V99.
       01  BaseAmount        PIC 9(5) VALUE 10555.
       01  PercentOfBase     PIC ZZ,ZZ9.99.
       01  PrnPercent        PIC ZZ9.

       PROCEDURE DIVISION.
       BEGIN.
           MOVE 23 TO PercentToApply
           COMPUTE PercentOfBase = BaseAmount * Percentage 
           DISPLAY "23% of 10555 is = " PercentOfBase 
           MOVE PercentToApply TO PrnPercent
           DISPLAY "Percentage applied was " PrnPercent "%"
           .
